from http.client import HTTPException
from fastapi import FastAPI
from services import UserService
from user import User

app = FastAPI()

@app.post("/users")
def create_user(user: User):
    UserService.create_user(user)
    return {"user_id": user.id}

@app.get("/users/{user_id}")
def get_user(user_id: int):
    user = UserService.get_user(user_id)
    if not user:
        raise HTTPException(status_code=404, detail="User not found")
    return user

from pydantic import BaseModel

class User(BaseModel):
    id: int
    name: str
    email: str
    password: str
    username: str
    cellphone: int
    account_balance: float

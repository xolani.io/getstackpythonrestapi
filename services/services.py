from user import User


class UserService:
    def __init__(self):
        self.users = {}
    
    def create_user(self, user: User):
        self.users[user.id] = user
    
    def get_user(self, user_id: int):
        return self.users.get(user_id)
